USE universityDB;

-- 1. Find the titles of courses in the Comp. Sci. department that have 3 credits.
SELECT title
FROM course
WHERE dept_name = 'Comp. Sci.'
AND credits = 3;

-- 2. Find the highest salary of any instructor.
SELECT MAX(salary)
FROM instructor;

-- 3. Find all instructors earning the highest salary (there may be more than one with the same salary).
SELECT ID, name
FROM instructor
WHERE salary = (SELECT MAX(salary) FROM instructor);

-- 4. Find the names of all instructors with salary between £90,000 and £100,000
SELECT name 
FROM instructor
WHERE salary BETWEEN 90000 AND 100000;

-- 5. Find the total number of instructors who teach a course in the Spring 2010 semester
SELECT COUNT(DISTINCT ID)
FROM teaches
WHERE semester = 'Spring'
AND year = 2010;

-- 6. Find the average salary of instructors in each department
SELECT dept_name, AVG(salary)
FROM instructor
GROUP BY dept_name;

-- 7. Find the names and average salaries of all departments whose average salary is greater than 42000
SELECT dept_name, AVG(salary)
FROM instructor
GROUP BY dept_name
HAVING AVG(salary) > 42000;

-- 8. Find names of instructors with salary greater than that of some (at least one) instructor in the Biology department.
SELECT name
FROM instructor
WHERE salary > 
	(SELECT MIN(salary) FROM instructor GROUP BY dept_name HAVING dept_name = 'Biology');

-- Find the average salary of instructors in the Computer Science department 
SELECT AVG(salary)
FROM instructor
GROUP BY dept_name
HAVING dept_name = 'Comp. Sci.';

-- Find the total number of instructors who teach a course in the Spring 2010 semester
SELECT COUNT(DISTINCT ID) 
FROM teaches t, section s
WHERE t.course_id = s.course_id
AND s.semester = 'Spring'
AND s.year = 2010;