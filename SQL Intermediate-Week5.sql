USE universityDB;
-- Exercise 1
-- a. Find the IDs of all students who were taught by an instructor named Einstein; make sure there are no duplicates in the result. 
SELECT DISTINCT takes.ID AS studentID, teaches.ID AS instructorID, instructor.name AS instructorName
FROM takes 
INNER JOIN teaches
USING (course_id, semester, year)
INNER JOIN instructor
ON teaches.ID = instructor.ID
WHERE instructor.name = 'Einstein';

-- b. Find all instructors earning the highest salary (there may be more than one with the same salary).
SELECT ID, name, salary
FROM instructor 
WHERE salary = (SELECT MAX(salary) FROM instructor);

-- e. Find the enrolment of each section that was offered in Autumn 2009.
SELECT takes.course_id, takes.sec_id, COUNT(DISTINCT takes.ID) AS enrolment
FROM takes
RIGHT JOIN 
	(SELECT * FROM section WHERE semester = 'Fall' AND year = 2009) AS section
USING (course_id, semester, year)
GROUP BY course_id, sec_id;

-- f. Find the maximum enrolment, across all sections, in Autumn 2009.
-- g. Find the sections that had the maximum enrolment in Autumn 2009.
SELECT takes.course_id, takes.sec_id, COUNT(DISTINCT takes.ID) AS enrolment
FROM takes
RIGHT JOIN 
	(SELECT * FROM section WHERE semester = 'Fall' AND year = 2009) AS section
USING (course_id, semester, year)
GROUP BY course_id, sec_id
ORDER BY enrolment DESC
LIMIT 1;

-- Exercise 2
-- a. Find the total grade-points earned by the student with ID 12345, across all courses taken by the student.
-- A = 4; A- = 3.7; B+ = 3.3; B = 3 and so on.
SELECT ID, SUM(grade_points) AS total_grade_points
FROM
	(SELECT ID, course_id, grade, CASE grade
		WHEN grade = 'A' THEN 4
		WHEN grade = 'A-' THEN 3.7
		WHEN grade = 'B+' THEN 3.3
		WHEN grade = 'B' THEN 3
		WHEN grade = 'B-' THEN 2.7
		WHEN grade = 'C+' THEN 2.4
		WHEN grade = 'C' THEN 2.1
		WHEN grade = 'C-' THEN 1.8
		WHEN grade = 'D+' THEN 1.5
		WHEN grade = 'D' THEN 1.2
		WHEN grade = 'D-' THEN 0.9
		WHEN grade = 'E+' THEN 0.6
		WHEN grade = 'E' THEN 0.3
		WHEN grade = 'E-' THEN 0.0
		END AS grade_points
	FROM takes) AS grade_table
GROUP BY ID
HAVING ID = '12345';

-- b. Find the grade-point average (GPA) for the above student, that is, the total grade-points divided by the total credits for the associated courses.
SELECT ID, total_weighted_grade_points/tot_credits AS GPA
FROM
	(SELECT ID, SUM(grade_points*credits) AS total_weighted_grade_points, SUM(credits) AS tot_credits
	FROM
		(SELECT ID, takes.course_id, grade, credits, CASE grade
			WHEN grade = 'A' THEN 4
			WHEN grade = 'A-' THEN 3.7
			WHEN grade = 'B+' THEN 3.3
			WHEN grade = 'B' THEN 3
			WHEN grade = 'B-' THEN 2.7
			WHEN grade = 'C+' THEN 2.4
			WHEN grade = 'C' THEN 2.1
			WHEN grade = 'C-' THEN 1.8
			WHEN grade = 'D+' THEN 1.5
			WHEN grade = 'D' THEN 1.2
			WHEN grade = 'D-' THEN 0.9
			WHEN grade = 'E+' THEN 0.6
			WHEN grade = 'E' THEN 0.3
			WHEN grade = 'E-' THEN 0.0
			END AS grade_points
	FROM takes
	LEFT JOIN course
	ON course.course_id = takes.course_id) AS grade_table
	GROUP BY ID) AS GPA_cal_table
HAVING ID = '12345';

-- c. Find the ID and the grade-point average of every student.
SELECT ID, total_weighted_grade_points/tot_credits AS GPA
FROM
	(SELECT ID, SUM(grade_points*credits) AS total_weighted_grade_points, SUM(credits) AS tot_credits
	FROM
		(SELECT ID, takes.course_id, grade, credits, CASE grade
			WHEN grade = 'A' THEN 4
			WHEN grade = 'A-' THEN 3.7
			WHEN grade = 'B+' THEN 3.3
			WHEN grade = 'B' THEN 3
			WHEN grade = 'B-' THEN 2.7
			WHEN grade = 'C+' THEN 2.4
			WHEN grade = 'C' THEN 2.1
			WHEN grade = 'C-' THEN 1.8
			WHEN grade = 'D+' THEN 1.5
			WHEN grade = 'D' THEN 1.2
			WHEN grade = 'D-' THEN 0.9
			WHEN grade = 'E+' THEN 0.6
			WHEN grade = 'E' THEN 0.3
			WHEN grade = 'E-' THEN 0.0
			END AS grade_points
	FROM takes
	LEFT JOIN course
	USING (course_id)) AS grade_table
	GROUP BY ID) AS GPA_cal_table;

-- Exercise 3
-- 1a. Display a list of all instructors, showing their ID, name, and the number of sections that they
-- have taught. Make sure to show the number of sections as 0 for instructors who have not taught
-- any section.
SELECT ID, name, COUNT(course_id)
FROM instructor
LEFT JOIN teaches
USING (ID)
LEFT JOIN course
USING (course_id)
GROUP BY ID;
    
-- 1b. Display the list of all departments, with the total number of instructors in each
-- department. Make sure to correctly handle departments with no instructors.
SELECT dept_name, COUNT(DISTINCT ID)
FROM department
LEFT JOIN instructor
USING (dept_name)
GROUP BY dept_name;

-- 2. Outer join expressions can be computed in SQL without using the SQL outer join operation. 
-- To illustrate this fact, show how to rewrite each of the following SQL queries without using the outer join expression
-- 2a. select * from student natural left outer join takes. 

-- SELECT * 
-- FROM student
-- NATURAL LEFT JOIN takes;

SELECT * 
FROM student
INNER JOIN takes
USING (ID)
UNION
SELECT ID, name, dept_name, tot_cred, NULL, NULL, NULL, NULL, NULL
FROM student s
WHERE NOT EXISTS (SELECT ID FROM takes t WHERE t.ID = s.ID);

-- 2b. select * from student natural full outer join takes.

-- SELECT * 
-- FROM student
-- LEFT JOIN takes
-- USING (ID)
-- UNION
-- SELECT * 
-- FROM student
-- RIGHT JOIN takes
-- USING (ID);

SELECT * 
FROM student
INNER JOIN takes
USING (ID)
UNION
SELECT ID, name, dept_name, tot_cred, NULL, NULL, NULL, NULL, NULL
FROM student s
WHERE NOT EXISTS (SELECT ID FROM takes t WHERE t.ID = s.ID)
UNION
SELECT ID, NULL, NULL, NULL, course_id, sec_id, semester, year, grade
FROM takes t
WHERE NOT EXISTS (SELECT ID FROM student s WHERE t.ID = s.ID);


