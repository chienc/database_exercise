CREATE DATABASE IF NOT EXISTS companyDB;
USE company;

CREATE TABLE employee (
	NI INT NOT NULL,
    bdate DATE NOT NULL,
    fname VARCHAR(14) NOT NULL,
    lname VARCHAR(16) NOT NULL,
    gender ENUM ('M', 'F') NOT NULL,
    hire_date DATE NOT NULL,
    job CHAR(15) NOT NULL,
    salary DECIMAL(14, 2) NOT NULL,
    dno INT NOT NULL,
    superNI INT NOT NULL,
    PRIMARY KEY (NI)
);

CREATE TABLE department (
	dnum INT NOT NULL,
    dname VARCHAR(40) NOT NULL,
    mgrNI INT NOT NULL,
    mgr_start_date DATE NOT NULL,
    FOREIGN KEY (mgrNI) REFERENCES employee (NI) ON DELETE CASCADE,
    PRIMARY KEY (dnum),
    UNIQUE KEY (dname)
);

CREATE TABLE dept_location (
	dno INT NOT NULL,
    dlocation CHAR(10) NOT NULL,
    FOREIGN KEY (dno) REFERENCES department (dnum) ON DELETE CASCADE,
    PRIMARY KEY (dno, dlocation)
);

CREATE TABLE project (
	pnum INT NOT NULL,
    pname CHAR(15) NOT NULL,
    plocation CHAR(10) NOT NULL DEFAULT 'Man',
    dno INT NOT NULL,
    FOREIGN KEY (dno) REFERENCES department (dnum) ON DELETE CASCADE,
    PRIMARY KEY (pnum)
);

CREATE TABLE works_on (
	eNI INT NOT NULL,
    pno INT NOT NULL,
    hours numeric(4, 2),
    FOREIGN KEY (eNI) REFERENCES employee (NI) ON DELETE CASCADE,
    FOREIGN KEY (pno) REFERENCES project (pnum) ON DELETE CASCADE,
    PRIMARY KEY (eNI, pno)
);

CREATE TABLE dependent (
	eNI INT NOT NULL,
    dependent_name CHAR(15) NOT NULL,
    gender ENUM('F', 'M') NOT NULL,
    bdate DATE NOT NULL,
    relationship CHAR(15) NOT NULL,
    FOREIGN KEY (eNI) REFERENCES employee (NI) ON DELETE CASCADE,
    PRIMARY KEY (eNI, dependent_name),
    UNIQUE KEY (dependent_name)
);

ALTER TABLE employee
ADD FOREIGN KEY (superNI) REFERENCES employee (NI) ON DELETE CASCADE,
ADD FOREIGN KEY (dno) REFERENCES department (dnum) ON DELETE CASCADE;

INSERT INTO employee (`NI`, `bdate`, `fname`, `lname`, `gender`, `hire_date`, `salary`,`Dno`,`superNI`,`job`) VALUES
(1, '1985-10-11','rogers','sanders','M','2016-08-02', '20000.00', 2, 4, 'developer'),
(2, '1985-10-04', 'mike','miller','M', '2016-11-02','30000.00' , 4, 4, 'not assinged'),
(3, '1950-10-11','david','brown','M','1999-10-11', '40000.00',1, 7, 'team leader'),
(4, '1942-10-11','ali','morgan','M','1975-10-11','70000.00',2, 8, 'manager'),
(5, '1986-10-08','john','wright','M', '2016-08-02','20000.00',4, 6, 'developer'),
(6, '1980-10-11','alex','john','F','2010-10-11', '45000.00',3, 9, 'project manager'),
(7,'1981-05-12', 'james','andrews','M','2011-10-05','60000.00',3, 9, 'manager'),
(8, '1940-08-07','jenny','ross', 'F','1980-10-11', '80000.00',1, 8, 'executive'),
(9, '1950-12-01','paul','bell','M', '1990-10-11','50000.00',2, 7, 'data analyst');

INSERT INTO department (`dnum`, `dname`, `mgrNI`, `mgr_start_date`) VALUES
(1, 'research',8, '2000-10-11'),
(2, 'hr',7, '2017-10-05'),
(3, 'technology',9, '2001-10-11'),
(4, 'payroll',4, '2000-03-11');

INSERT INTO project VALUES
(1, 'Project A', 'MediaCity', 1),
(2, 'Project B', 'MediaCity', 3),
(3, 'Project C', 'MediaCity', 4),
(4, 'Project D', 'MediaCity', 3),
(5, 'Project E', 'MediaCity', 3),
(6, 'Project F', 'MediaCity', 3),
(7, 'Project G', 'MediaCity', 1),
(8, 'Project H', 'MediaCity', 1),
(9, 'Project I', 'MediaCity', 1),
(10, 'Project J', 'MediaCity', 2),
(11, 'Project K', 'MediaCity', 2),
(12, 'Project L', 'MediaCity', 1),
(1departmentdepartment3, 'Project M', 'MediaCity', 4),
(14, 'Project N', 'MediaCity', 3),
(15, 'Project O', 'MediaCity', 3);

INSERT INTO works_on VALUES
(1, 1, 4.25),
(1, 2, 4.25),
(1, 3, 4.25),
(2, 1, 4.25),
(2, 4, 4.25),
(2, 5, 4.25),
(2, 6, 4.25),
(2, 7, 4.25),
(3, 3, 4.25),
(3, 4, 4.25),
(3, 5, 4.25),
(3, 6, 4.25),
(4, 3, 4.25),
(4, 5, 4.25),
(5, 7, 4.25),
(6, 8, 4.25),
(6, 9, 4.25),
(7, 10, 4.25),
(7, 11, 4.25),
(8, 12, 4.25),
(9, 13, 4.25),
(9, 14, 4.25),
(9, 15, 4.25);