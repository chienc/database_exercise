-- Count the number of distinct salary values in the database.
SELECT COUNT(DISTINCT salary)
FROM employee;

-- Find the names and average salaries of all departments whose average salary is greater than 42000 (Company DB)
SELECT dname, AVG(e.salary) AS avg_salary
FROM employee e, department d
WHERE e.dno = d.dnum
GROUP BY dname
HAVING avg_salary > 42000;

-- For each department, retrieve the department number, the number of employees in the department, and their average salary (Company DB)
SELECT dnum, COUNT(NI) AS employee_number, AVG(salary) AS avg_salary
FROM employee, department
WHERE dno = dnum
GROUP BY dnum
ORDER BY dnum;

-- For each project, retrieve the project number, the project name, and the number of employees who work on that project (Company DB)
SELECT pnum, pname, COUNT(eNI)
FROM project p, works_on w
WHERE p.pnum = w.pno
GROUP BY pnum
ORDER BY pnum;

-- For each project on which more than two employees work, retrieve the project number, the project name, and the number of employees who work on the project.
SELECT pnum, pname, COUNT(eNI)
FROM works_on w, project p
WHERE p.pnum = w.pno
GROUP BY pnum
HAVING COUNT(eNI) > 2;

-- Find the sum of the salaries of all employees of the ‘Research’ department, as well as the maximum salary, the minimum salary, and the average salary in this department.
SELECT SUM(salary), MAX(salary), MIN(salary), AVG(salary)
FROM employee, department
WHERE dnum = dno 
AND dname = 'Research';

-- Retrieve the total number of employees in the company and the number of employees in the ‘Research’ department.
SELECT SUM(NI),
	(SELECT COUNT(NI) FROM employee, department WHERE dno = dnum GROUP BY dname HAVING dname = 'Research')
    AS employee_in_research
FROM employee;

-- Retrieve the national insurance numbers of all employees who work on project number 1, 2, or 3.
SELECT eNI 
FROM works_on
WHERE pno IN (1, 2, 3);

-- For each department having more than five employees, retrieve the department number and the number of employees making more than £40,000.
SELECT e.dno, subquery.employee_number
FROM employee e,
	(SELECT dno, COUNT(NI) AS employee_number FROM employee GROUP BY dno HAVING COUNT(NI) > 5) 
    AS subquery
WHERE e.dno = subquery.dno
AND e.salary > 40000;