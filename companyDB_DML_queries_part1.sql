USE companyDB;
-- a. Retrieve the names of all employees in department 5 who work more than 10 hours per week on the “Product Mega” project.
SELECT fname, lname 
FROM employee e, works_on w, project p
WHERE e.NI = w.eNI AND w.pno = p.pnum
AND pname = 'Product Mega'
AND e.dno = 5;

-- b. Show the resulting salaries if every employee working on the ‘Product Alfa’ project is given a 10% raise
SELECT fname, lname, salary*1.1 AS raised_salary
FROM employee, project, works_on
WHERE NI = eNI AND pno = pnum
AND pname = 'Product Alfa';

-- c. List the names of all employees who have a dependent with the same first name as themselves.
SELECT fname, lname
FROM employee, dependent
WHERE NI = eNI
AND fname = substring_index(dependent_name, ' ', 1);

-- d. Retrieve a list of employees and the projects they are working on, ordered by department and, within each department, alphabetically by last name, first name.
SELECT d.dname , e.lname, e.fname, p.pname
FROM employee e, works_on w, department d, project p
WHERE e.NI = w.eNI AND e.dno = d.dnum AND w.pno = p.pnum
ORDER BY d.dname, e.lname, e.fname;

-- e. Find the names of all employees who are directly supervised by ‘Franklin Wong’.
SELECT e1.NI, e1.fname, e1.lname
FROM employee e1, employee e2
WHERE e1.superNI = e2.NI
AND e2.fname = 'Franklin'
AND e2.lname = 'Wong';